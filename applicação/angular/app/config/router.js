angular.module('App').config([
	'$stateProvider',
	'$urlRouterProvider',
	function($stateProvider, $urlRouterProvider ){
		$stateProvider.state('dashboard',{
			url: '/dashboard',
			templateUrl: "dashboard/dashboard.html"
		});
		$stateProvider.state('ciclo',{
			url:'/ciclo?page',
			templateUrl:'Ciclo/pagamento.html'
		});

		$urlRouterProvider.otherwise('/dashboard')	
	}
]);