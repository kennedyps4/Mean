( () => {
	angular.module('App').controller('DashboardCtrl',['$http', DashboardCtrl ])

	function DashboardCtrl($http){
		let vm = this;
		 this.$http = $http;


		vm.getSummary = () =>{
			const url ='http://localhost:3003/api/billySumary';

			this.$http.get(url).then( ( response)=>{
				const {credits =0, debts =0 } = response.data
				vm.credits = credits;
				vm.debts = debts;
				vm.total = credits - debts;
			});
		}
		vm.getSummary();
	}
})()