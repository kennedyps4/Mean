( ()=>{

	angular.module('App').controller('CicloCtrl',['$http','msgs','tabs','$location',CicloCtrl ])

	function CicloCtrl($http, msgs, tabs, $location){
		const vm = this;
		this.$http = $http;

		const url = 'http://localhost:3003/api/billingCycles';

		vm.refresh = () =>{
			const page =  parseInt( $location.search().page) || 1
			
			$http.get( `${url}?skip=${ (page - 1) * 10 }&limit=10` ).then( (result)=>{
				vm.ciclo = {credits:[{}], debts:[{}]};
				vm.ciclos = result.data
				vm.calculateValues();
				$http.get(`${url}/count`).then( (response)=>{

					vm.pages = Math.ceil(response.data.value / 10)
				});	
				
				tabs.show(vm,{tabList: true, tabCreate: true})

			})
		}

		// Insert
		vm.create = () => {		

			$http.post(url, vm.ciclo ).then( (result)=>{
				msgs.addSuccess('Operação realizada com Sucesso!!')
			},
			//error
			(data) => {
				msgs.addError(data.data.errors)
			})
			vm.refresh()
		}


		vm.showTabUpdate = ( ciclo) => {

			vm.ciclo = ciclo;
			tabs.show(vm, {tabUpdate: true})
			vm.calculateValues();
		};

		vm.showTabDelete = ( ciclo) => {
			vm.calculateValues();
			vm.ciclo = ciclo;
			tabs.show(vm, {tabDelete: true})
		};

		vm.Delete = () => {
			const urlDelete = `${url}/${vm.ciclo._id}`;

			$http.delete( urlDelete, vm.ciclo).then( (result) =>{
				vm.refresh();
				msgs.addSuccess('Registro excluido com Sucesso!!')
			}, 
			(error)=>{
				msgs.addError(error.data.errors)
			})
		}


		vm.Edit = () => {
			const urlEdit = `${url}/${vm.ciclo._id}`;
			$http.put( urlEdit, vm.ciclo).then( (result) =>{
				vm.refresh();
				msgs.addSuccess('Registro Editado com Sucesso!!')
			}, 
			(error)=>{
				console.log(error)
				msgs.addError(error.data.errors)
				
			})
		}

		/* Açoes creditos e debitos */


		vm.addCredito = ( index ) => {
			vm.ciclo.credits.splice(index+1, 0, {} );
			vm.calculateValues();
		}

		vm.cloneCredito = (index, {name, value}) => {
			vm.ciclo.credits.splice( index + 1, 0, {name, value});
			vm.calculateValues();
		}

		vm.creditoDelete = (index) => {
			if( vm.ciclo.credits.length > 1){
				vm.ciclo.credits.splice(index,1);
				vm.calculateValues();
			}
		}

		/* Debitos*/

		vm.addDebito = ( index ) => {
			vm.ciclo.debts.splice(index+1, 0, {} );
			vm.calculateValues();
			
		}

		vm.cloneDebito = (index, {name, value, status}) => {
			vm.ciclo.debts.splice( index + 1, 0, {name, value, status} );
			vm.calculateValues();
		}

		vm.debitoDelete = (index) => {
			if( vm.ciclo.debts.length > 1){
				vm.ciclo.debts.splice(index,1);
				vm.calculateValues();
			}
		}

		vm.calculateValues = ()=>{
			vm.soma = '';
			vm.diminue = '';

			if( vm.ciclo){
				vm.ciclo.credits.forEach( ( {value} ) => {
					vm.soma += !value || isNaN(value)? 0 : parseFloat( value );
				});

				vm.ciclo.debts.forEach( ( {value} ) => {
					vm.diminue += !value || isNaN(value)? 0: parseFloat(value)
				});
			
				vm.result = vm.soma  - vm.diminue;
			}
		}

		vm.refresh();
	}
})()