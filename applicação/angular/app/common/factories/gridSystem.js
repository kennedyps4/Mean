 /*'12' -> col-xs-12
 '12 6' -> col-xs-12 col-sm-12
 '12 6 3 2 ' col-xs-12 col-sm-6 col-md-3 col-lg-2*/

 angular.module('App').factory('gridSystem', [function(){

 	function toCssClasses(numbers='12'){
 		const cols = numbers.split(' ');
 		let classes = '';

 		if( cols[0]){ classes+=` col-xs-${cols[0]}` }
 		if( cols[1]){ classes+=` col-sm-${cols[1]}` }
 		if( cols[2]){ classes+=` col-md-${cols[2]}` }
 		if( cols[3]){ classes+=` col-lg-${cols[3]}` }
 		
 		return classes;
 	};

 	return { toCssClasses }
 }])
