( ()=>{
	angular.module('App').component('paginate',{
		bindings:{
			url: '@',
			pages: '@',
		},
		controller: ['$location', function($location){

			console.log(this.pages)
			this.current =parseInt( $location.search().page ) || 1;
				const pages = parseInt(this.pages) || 1
			this.$onInit = function(){
				this.arraPages = Array(pages).fill(0).map( (e,i)=>{  return i+1});
							
				this.needPaginate  = this.pages > 1;
				this.hasPrev = this.current > 1;
				this.hasNext = this.current <= this.pages;
			}
				
				this.isCurrent = function(i){
					return this.current == i;
				}

		}],
		template:`
			<ul ng-if="$ctrl.needPaginate" class="pagination pagination-sm no-margin pull-right">
				<li ng-if="$ctrl.hasPrev">
					<a href="{{ $ctrl.url }}?page={{ $ctrl.current - 1 }}"> Anterior </a>
				</li>
		
				<li  ng-repeat=" index in $ctrl.arraPages">

					<a href="{{ $ctrl.url }}?page={{index}}"> {{ index }} </a>
				</li>
			
				<li ng-if="$ctrl.hasNext">
					<a href="{{ $ctrl.url}}?page={{$ctrl.current + 1}}">Proximo</a>
				</li>
			</ul>`
		
	});
})()