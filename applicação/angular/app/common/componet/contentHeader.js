angular.module('App').component('contentHeader',{
	bindings:{
		name: '@',
		small: '@',
	},
	template: `
	<section>
		<h3> {{::$ctrl.name}} <small> {{::$ctrl.small}}</small></h3>
	</section>`
})