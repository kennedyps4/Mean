const _ = require('lodash');
const BillingCycle = require('../billyCycles/BillingCycle');

// middleware para sumaryzar as requisições

function getSumary(req ,res){
	BillingCycle.aggregate({
		$project:{ credits: {$sum: '$credits.value'}, debts:{$sum: "$debts.value"}}
	}, {
		$group:{ _id: null, credits:{$sum:"$credits"}, debts: {$sum: "$debts"} }
	}, {
		$project:{_id:0, credits:1, debts:1}		
	}, function(err, result){
		if(err){
			res.status(500).json({errors: [err]})
		} else {
			res.json( _.defaults(result[0], {credits:0, debts:0}) )
		}	
	});
}

module.exports = { getSumary };