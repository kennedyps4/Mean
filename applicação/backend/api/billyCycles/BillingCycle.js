const restful = require('node-restful');
const mongoose = restful.mongoose;

/* shema de creditos*/
const creditShema = new mongoose.Schema({
	name:{ type: String, required: true },
	value:{ type:Number, min:0, required:true }
});


/* shema de debitos*/
const debtSchema = new mongoose.Schema({
	name:{ type: String, required: true },
	value:{ type: Number, min: 0, required:true },
	status:{ type: String, required:true, uppercase:true,
		enum:['PAGO', 'PENDENTE', 'AGENDADO', 'CANCELADO']
	 }
});

/* shema de ciclo de creditos e debitos*/
const billingCycleSchema = new mongoose.Schema({
	name:{ type: String, required:true},
	month:{ type: Number, min:1, max:12, required: [true, 'Informe o valor do Debito'] },
	year:{ type: Number, min:1970, max:3000, required:true},
	credits: [creditShema],
	debts: [debtSchema]	
});

module.exports = restful.model('BillingCycle', billingCycleSchema );
