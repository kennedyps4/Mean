const _ = require('lodash');
const BillingCycle = require('./BillingCycle');

/* methods da rota */
BillingCycle.methods(['get','post','put','delete']);
/*retorna dado atualizado*/
BillingCycle.updateOptions({new:true, runValidators:true});


/* interceptar requisição  antes de ser processada ou depois */

BillingCycle.after('post', sendErrorsOrNext).after('put', sendErrorsOrNext)

function sendErrorsOrNext(req, res, next){
	const bundle = res.locals.bundle

	if(bundle.errors){
		
		let errors = parseErrors(bundle.errors);
		res.status(500).json({errors})

	} else{
		
		next()
	}
}

/* Formata erros do mongodb para um padrao */
function parseErrors(nodeRestfullErrors){
	const errors = [];
	_.forIn(nodeRestfullErrors, error => errors.push( error.message));

	return errors;
}

BillingCycle.route('count', (req, res, next)=>{
	// consulta mongo
	BillingCycle.count( (err, value ) => {  
		if(err) {
			res.status(500).json({errors:[err]});
		} else {
			res.json({value})
		}
	});
});


module.exports = BillingCycle;