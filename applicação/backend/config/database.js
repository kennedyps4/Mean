const mongoose = require('mongoose');

mongoose.Error.messages.general.required = "O atributo '{PATH} é obrigatório'";
mongoose.Error.messages.Number.min = "O {value} informado é menor que o limite maximo de {MIN}";
mongoose.Error.messages.Number.max = "O {value} informado é maior que o limite maximo de {MAX}";
mongoose.Error.messages.String.enum = " {value} não é válido para o atributo {PATH}";

module.exports = mongoose.connect('mongodb://localhost/db_finance')
//module.exports = mongoose.connect('mongodb://usuario:senha@localhost:port/db_finance')
