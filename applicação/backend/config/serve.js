const bodyParser = require('body-parser');
const express = require('express');
const server = express();
const cors = require('cors');
const queryParse = require('express-query-int');
const port = 3003;

server.use( bodyParser.urlencoded( { extends: true } ));

server.use( bodyParser.json())

server.use(cors());
server.use(queryParse())

server.listen(port, ()=> console.log(`Backend is running on port ${port}.`))

module.exports = server;