const express = require('express');

module.exports = function (server){
	const router = express.Router();

	//nome da rota padrao
	server.use('/api', router);

	/*Rotas da API */
	const billingCycle = require('../api/billyCycles/billingCycles_Service')
	billingCycle.register( router , '/billingCycles')

	const billySumary = require('../api/billySumary/billySumaryService');
	router.route('/billySumary').get( billySumary.getSumary)
}	
